function regExpMatch(url, pattern) {
	try { return new RegExp(pattern).test(url); } catch(ex) { return false; }
}

function FindProxyForURL(url, host) {
    if (shExpMatch(url, '*.blogger.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.blogspot.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*www.google.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*mail.google.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*news.google.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*reader.google.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*plus.google.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*images.google.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.googleusercontent.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.appspot.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.googlesyndication.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.feedburner.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.youtube.*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*youtu.be*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.googlecode.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.gstatic.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.googleapis.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*goo.gl*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*translate.google.cn*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*accounts.google.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*drive.google.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*groups.google.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*code.google.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*docs.google.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*www.google.com/imgres*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*www.google.com/analytics*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*admin.google.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*googlevideo.com*')) return 'PROXY 127.0.0.1:8087';

    if (shExpMatch(url, '*stackauth.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*cdn.sstatic.net*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*stackexchange.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*stackoverflow.com*')) return 'PROXY 127.0.0.1:8087';

    if (shExpMatch(url, '*twimg.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*twitter.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*twitterfeed.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*ytimg.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*twitpic.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*img.ly*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, 'http://t.co*')) return 'PROXY 127.0.0.1:8087';

    if (shExpMatch(url, '*bit.ly*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*j.mp*')) return 'PROXY 127.0.0.1:8087';

    if (shExpMatch(url, '*wordpress.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.wp.com*')) return 'PROXY 127.0.0.1:8087';

    if (shExpMatch(url, '*.wikipedia.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.wikipedia.org*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.wikimedia.org*')) return 'PROXY 127.0.0.1:8087';

    if (shExpMatch(url, '*.facebook.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.fbcdn.net*')) return 'PROXY 127.0.0.1:8087';

    if (shExpMatch(url, '*.quora.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.quoracdn.net*')) return 'PROXY 127.0.0.1:8087';

    if (shExpMatch(url, '*.amazon.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.images-amazon.com*')) return 'PROXY 127.0.0.1:8087';

    if (shExpMatch(url, '*.flickr.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.staticflickr.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.yahoo.com*')) return 'PROXY 127.0.0.1:8087';

    if (shExpMatch(url, '*instagram.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*instagr.am*')) return 'PROXY 127.0.0.1:8087';

    if (shExpMatch(url, '*hulu.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*huluim.com*')) return 'PROXY 127.0.0.1:8087';

    if (shExpMatch(url, '*linkedin.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*licdn.com*')) return 'PROXY 127.0.0.1:8087';

    if (shExpMatch(url, '*.wxpython.org*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.python.org*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*dropbox.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.imdb.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*vimeo.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*toodledo.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*cloudflare.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*cloudfront.net*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*posterous.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*topsy.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*foursquare.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.time.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*gravatar.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.w3schools.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.atlassian.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*.goodreads.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*colourlovers.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*unbit.it*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*scriptogr.am*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*www.slideshare.net/*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*www.sourceforge.net*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*godaddy.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*akamai.net*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*github.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*github.io*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*feedly.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*golang.org*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*tumblr.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*trello.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*trellocdn.com*')) return 'PROXY 127.0.0.1:8087';
    if (shExpMatch(url, '*fastly.net*')) return 'PROXY 127.0.0.1:8087';

    return 'DIRECT';
}

